IMG_NAME=index.docker.io/geraudster/svg2pdf
FORMAT=pdf
PAGE_ORDER="12,1-11,13"
#PAGE_ORDER="-"
DPI=300
SUFFIX=

build:
	docker build -t $(IMG_NAME) .

run:
	docker run --rm -v $(PWD)/input.zip:/home/inkscape/input.zip -v $(PWD)/output/:/home/inkscape/output/ -it $(IMG_NAME) ./convert.sh $(DPI) $(FORMAT) $(PAGE_ORDER) "$(SUFFIX)"

run-override:
	docker run --rm -v $(PWD)/input.zip:/home/inkscape/input.zip -v $(PWD)/output/:/home/inkscape/output/ -v $(PWD)/convert.sh:/home/inkscape/convert.sh -it $(IMG_NAME) ./convert.sh $(DPI) $(FORMAT) $(PAGE_ORDER) "$(SUFFIX)"
#	docker run --rm -v $(PWD)/input.zip:/home/inkscape/input.zip -v $(PWD)/output/:/home/inkscape/output/ -v $(PWD)/convert.sh:/home/inkscape/convert.sh -it $(IMG_NAME) ./convert.sh 300 $(FORMAT) $(PAGE_ORDER) "$(SUFFIX)"
