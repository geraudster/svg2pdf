#!/usr/bin/env bash

set -euxo pipefail

DPI=$1
FORMAT=$2
PAGE_ORDER=$3
SUFFIX=$4
TMPDIR=$(mktemp -d)

# Relative to current directory
PARTIAL_OUTPUT=BIM_${DPI}dpi_${FORMAT}${SUFFIX}
OUTPUT_DIR=output/${PARTIAL_OUTPUT}/

unzip input.zip -d "$TMPDIR"
pushd $TMPDIR
echo "Rename directories"
find . -type d -print0 | xargs -0 rename -e 's/^(.*)\/([^\/]*)$/$1\/\L$2/; s/ //g'

echo "Rename files"
find . -name \*.svg -print0 | xargs -0 rename -e 's/^(.*)\/([^\/]*)$/$1\/\L$2/; s/[() ]//g'

ls -R
popd
find $TMPDIR/ -name \*.svg -print0 | xargs -t -0 -l inkscape --export-text-to-path --export-type=$FORMAT --export-dpi=$DPI -b white

function split_pdf {
    FILENAME=$1
    echo -n "$FILENAME "
    if [[ $FILENAME =~ ^(.*page)([0-9]{2})_([0-9]{2})(.*\.pdf)$ ]]; then
        FIRST_PAGE="${BASH_REMATCH[1]}${BASH_REMATCH[2]}${BASH_REMATCH[4]}"
        SECOND_PAGE="${BASH_REMATCH[1]}${BASH_REMATCH[3]}${BASH_REMATCH[4]}"

        echo $FIRST_PAGE and $SECOND_PAGE
        pdfseparate -f 1 -l 1 $FILENAME $FIRST_PAGE
        pdfseparate -f 2 -l 2 $FILENAME $SECOND_PAGE
        mv "$FILENAME" "${FILENAME}.bak"
    else
        echo "No match"
    fi
}

find $TMPDIR -regextype posix-extended -iregex '.*page[0-9]{2}_[0-9]{2}.*\.pdf' -print0 | while IFS= read -r -d '' file; do split_pdf "$file"; done
find $TMPDIR -name \*.pdf -print 2> /dev/null || true
ls -R $TMPDIR
mkdir -p "$OUTPUT_DIR"

find $TMPDIR/ -name \*.$FORMAT -exec mv '{}' "$OUTPUT_DIR" ';'

cd "$OUTPUT_DIR"

if [[ $FORMAT == "pdf" ]]; then
    pdfunite $(ls -v *.pdf) BIM_${DPI}dpi${SUFFIX}.pdf
    pdfjam --suffix 2x1 --nup '2x1' --landscape -- BIM_${DPI}dpi${SUFFIX}.pdf ${PAGE_ORDER}
    mv BIM_${DPI}dpi*-2x1.pdf BIM_${DPI}dpi${SUFFIX}_2x1.pdf
elif [[ $FORMAT == "png" ]]; then
    eval convert $(ls -v) BIM_${DPI}dpi_${FORMAT}${SUFFIX}.pdf
fi

zip -r ../${PARTIAL_OUTPUT} .
rm -rf "$TMPDIR"
