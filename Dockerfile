FROM ubuntu:20.04 as BUILD
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y wget
RUN wget -v https://gitlab.com/inkscape/inkscape-ci-docker/-/raw/master/install_dependencies.sh -O install_dependencies.sh
RUN bash install_dependencies.sh --recommended

RUN wget -v https://media.inkscape.org/dl/resources/file/inkscape-1.2.tar.xz -O inkscape-1.2.tar.xz
RUN tar xJf inkscape-1.2.tar.xz && rm -f inkscape-1.2.tar.xz && mv inkscape-1.2_* inkscape-1.2
RUN mkdir inkscape-1.2/build
WORKDIR inkscape-1.2/build

RUN cmake .. -DCMAKE_INSTALL_PREFIX=/opt/inkscape -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_CXX_COMPILER_LAUNCHER=ccache
RUN make -j8
RUN mkdir -p /opt/inkscape
RUN make install

FROM ubuntu:20.04

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y locales
RUN sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
ENV LC_ALL fr_FR.UTF-8

RUN DEBIAN_FRONTEND=noninteractive apt install -y  \
    software-properties-common \
    poppler-utils \
    fonts-comfortaa \
    culmus \
    texlive-extra-utils \
    zip unzip rename curl \
    imagemagick \
    aspell \
    libwmf-bin \
    perlmagick \
    python3-pip \
    cython \
    python3-numpy \
    python3-pil \
    python3-lxml \
    python3-serial \
    python3-scour \
    python3-packaging \
    libatkmm-1.6-1v5 \
    libboost-filesystem1.71.0 \
    libcairo-gobject2 \
    libcairomm-1.0-1v5 \
    libcdr-0.1-1 \
    libdouble-conversion3 \
    libgc1c2 \
    libgdk-pixbuf2.0-0 \
    libglibmm-2.4-1v5 \
    libgsl23 \
    libgspell-1-2 \
    libgtk-3-0 \
    libgtkmm-3.0-1v5 \
    libmagick++-6.q16-8 \
    libpangomm-1.4-1v5 \
    libpoppler-glib8 \
    libpotrace0 \
    librevenge-0.0-0 \
    libsigc++-2.0-0v5 \
    libvisio-0.1-1 \
    libwpg-0.3-3

COPY --from=BUILD /opt /opt/
ENV PATH=$PATH:/opt/inkscape/bin
ARG user=inkscape
ARG INKSCAPE_HOME=/home/inkscape

RUN useradd -ms /bin/bash ${user}

COPY convert.sh $INKSCAPE_HOME

VOLUME $INKSCAPE_HOME/input
VOLUME $INKSCAPE_HOME/output

RUN sed -i.bak '/pattern="PDF"/d' /etc/ImageMagick-6/policy.xml
RUN sed -i.bak 's!<policy domain="resource" name="disk" value="1GiB"/>!<policy domain="resource" name="disk" value="8GiB"/>!' /etc/ImageMagick-6/policy.xml
USER ${user}
WORKDIR ${INKSCAPE_HOME}
